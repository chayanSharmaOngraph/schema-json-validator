const validator = require('jsonschema').Validator;
const v = new validator();

const { productStructureSchema } = require('./schemas/schema');
const { componentSchema } = require('./schemas/schema');
const { fieldsSchema } = require('./schemas/schema');
const { commonInputsSchema } = require('./schemas/schema');
const { radioInputSchema } = require('./schemas/schema');
const { groupedInputSchema } = require('./schemas/schema');

const previewProductConfig = {
	"productId": 1,
	"productTitle": "Preview",
	"components": [{
			"step": 1,
			"fields": [{
				"id": "Q1",
				"inputType": "textarea",
				"fieldTitle": "First please tell us about the Marketing Job to be Done and the task that the agency was asked to do. Please also provide any relevant information on market or brand context.",
				"fieldName": "objectiveBrief",
				"validations": [{
					"name": "required",
					"value": true,
					"message": "Field cannot be empty."
				}]
			}]
		},
		{
			"step": 2,
			"header": "Now some questions on the scope of the test.",
			"fields": [{
					"id": "Q2",
					"inputType": "select",
					"fieldTitle": "Country of Fieldwork",
					"hasDependent": true,
					"fieldName": "countries",
					"multi": true,
					"autoFill": true,
					"service": "getAllCountries",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"values": [{
							"id": 21,
							"name": "india"
						},
						{
							"id": 22,
							"name": "australia"
						}
					]

				},
				{
					"id": "Q3",
					"inputType": "number",
					"fieldTitle": "Number of Ads to be tested",
					"fieldName": "countOfAds",
					"max": 5,
					"validations": [{
							"name": "required",
							"value": true,
							"message": "Field cannot be empty."
						},
						{
							"name": "max",
							"value": 5,
							"message": "Value is not greater than max"
						}
					],
					"hasDependent": true
				},
				{
					"id": "Q4",
					"inputType": "date",
					"fieldTitle": "When do you want the Report to be delivered by?",
					"fieldName": "deliveryDate",
					"disablePreviousDate": true,
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q5",
					"inputType": "date",
					"fieldTitle": "Stimulus needs to be delivered by",
					"fieldName": "stimulusDate",
					"rule": "moment(this.studyForm.value['deliveryDate']).subtract(15 , 'day')",
					"autoFill": true,
					"disabled": true,
					"condition": "(new Date() - moment(this.studyForm.value['deliveryDate']).subtract(15 , 'day')>0)"
				}
			]
		},
		{
			"step": 3,
			"header": "Now tell us about the project",
			"fields": [{
					"id": "Q6",
					"inputType": "text",
					"fieldTitle": "Unilever Project Name",
					"fieldName": "projectName",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q7",
					"inputType": "select",
					"fieldTitle": "Category",
					"fieldName": "productCategory",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"multi": false,
					"service": "getAllProductCategory",
					"dependent": "Q8",
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum 1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum 2"
						}
					]
				},
				{
					"id": "Q8",
					"inputType": "select",
					"fieldTitle": "Sub-Category",
					"fieldName": "productSubCategory",
					"service": "getAllSubCategory",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"multi": false,
					"selectedOptions": "this.studyForm.value['productCategory']",
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum 1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum 2"
						}
					]
				},
				{
					"id": "Q9",
					"inputType": "select",
					"fieldTitle": "Global Brand name",
					"fieldName": "globalBrand",
					"service": "getAllBrands",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"multi": false,
					"dependent": "Q10",
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum 1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum 2"
						}
					]
				},
				{
					"id": "Q10",
					"inputType": "select",
					"fieldTitle": "Local Parent Brand name",
					"subTitle": "(English)",
					"fieldName": "localBrandEng",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"multi": false,
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
						}
					]
				},
				{
					"id": "Q11",
					"inputType": "text",
					"fieldTitle": "Complete Variant brand name",
					"subTitle": "(English)",
					"fieldName": "completeBrandEng",
					"validations": {
						"required": false
					}
				}
			]
		},
		{
			"step": 4,
			"header": "Now please provide some details about each of the ads",
			"fields": [{
					"id": "Q12",
					"hidden": true,
					"parentId": "countOfAds",
					"condition": "this.studyForm.value['countOfAds']",
					"inputType": "grouped",
					"fieldTitle": "Ad",
					"fieldName": "ad_Counts",
					"totalCount": "this.studyForm.value['countOfAds']",
					"subFields": [{
							"id": "Q12_1",
							"inputType": "text",
							"fieldTitle": "Ad name",
							"fieldName": "adName",
							"validations": [{
								"name": "required",
								"value": true,
								"message": "Field cannot be empty."
							}]
						},
						{
							"id": "Q12_2",
							"inputType": "select",
							"fieldTitle": "Finished film or Unfinished",
							"fieldName": "film",
							"multi": false,
							"validations": [{
								"name": "required",
								"value": true,
								"message": "Field cannot be empty."
							}],
							"values": [{
									"id": 1,
									"name": "Finished film"
								},
								{
									"id": 2,
									"name": "Animatic"
								}
							]
						},
						{
							"id": "Q12_3",
							"inputType": "number",
							"fieldTitle": "Ad length ( in seconds)",
							"fieldName": "adLength",
							"validations": [{
									"name": "required",
									"value": true,
									"message": "Field cannot be empty"
								},
								{
									"name": "max",
									"value": 300,
									"message": "Value is not greater than max"
								},
								{
									"name": "min",
									"value": 5,
									"message": "value is not less than min value"
								}
							]
						}
					]
				},
				{
					"id": "Q13",
					"inputType": "select",
					"fieldTitle": "To what degree is the ad intended to focus on purpose?",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"multi": false,
					"fieldName": "completeBrandLocal",
					"values": [{
							"id": 1,
							"name": "<b>Pure purpose:</b> The main intent of the ad is to convey the brand’s purpose. e.g. Dove Sketches"
						},
						{
							"id": 2,
							"name": "<b>Product/Category enabled purpose:</b> The ad is intending to convey the brand’s purpose alongside product or category specific functions e.g. Vaseline healing project, Dove your hair your choice."
						},
						{
							"id": 3,
							"name": "<b>Purpose as context not a focus:</b> The intent of the ad does not focus on the brand’s purpose but it is there in an implicit, indirect way for example through the use of the brand’s slogan or executional elements that cue the brand’s purpose."
						},
						{
							"id": 4,
							"name": "<b>No purpose:</b> The ad has no reference to purpose at all, neither implicit nor explicit."
						}
					]
				}
			]
		},
		{
			"step": 5,
			"header": "And now for some inputs on research design",
			"fields": [{
					"id": "Q14",
					"inputType": "textarea",
					"fieldTitle": "Sample definition –",
					"fieldName": "sampleDefinition",
					"icon": "assets/images/modal_open.png",
					"description": "*Standard sample definitions are included in the Relevant Sample Master Sheet on the Unilever Portal",
					"autoFill": true,
					"disabled": true
				},
				{
					"id": "Q15",
					"inputType": "radio",
					"fieldTitle": "Standard sample definition?",
					"fieldName": "wantStandardDefinition",
					"hasDependent": true,
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					],
					"multi": false
				},
				{
					"id": "Q16",
					"parentId": "wantStandardDefinition",
					"inputType": "text",
					"fieldTitle": "IF NO, CONFIRM WITH CMI AND specify sample",
					"fieldName": "alternateSampleDefinition",
					"condition": "this.studyForm.value['wantStandardDefinition'] == false",
					"hidden": true,
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				}
			]
		},
		{
			"step": 6,
			"hidden": true,
			"parentId": "countries",
			"header": "Key Messages (For all fieldwork markets with the exception of Indonesia) \n  Insert first three key messages in the order of importance ",
			"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) == -1",
			"fields": [{
					"id": "Q17",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) == -1",
					"inputType": "grouped",
					"fieldName": "keyMessageHidden",
					"fieldTitle": "Insert first three key messages in the order of importance",
					"icon": "assets/images/modal_open.png",
					"totalCount": 3,
					"maxCount": 4,
					"description": "Key message: these are the statements we will use to measure the single point we want our consumers to take out of the ad. It can be helpful to refer back to the creative brief to make sure this reflects the communication objective given to the agency. Think about the most important thing you want consumers to take out of the ad, e.g the benefit a new product formula brings to the consumer might be more important than recalling a specific name or ingredient of the new formula. The list is already populated with the Must Win Attributes (MWA's) for your brand. You can add other statements (up to a max of 6) if the MWAs do not cover the main point in the ad brief. Other tips: make statements distinct from each other as possible; avoid generic benefits (e.g. is effective, is a trustworthy brand) unless a specific communication objective. Include either parent or variant brand name in each statement, do not mix the two; keep statements short and use consumer language.",
					"subFields": [{
						"id": "Q17_1",
						"inputType": "text",
						"fieldName": "keyMessage",
						"fieldTitle": "Key message",
						"validations": [{
							"name": "required",
							"value": true,
							"message": "Field cannot be empty."
						}]
					}]
				},
				{
					"id": "Q18",
					"inputType": "button",
					"fieldName": "",
					"showBtn": true,
					"fieldTitle": "Add More",
					"addMore": "keyMessageHidden",
					"subFields": {
						"id": "Q17_1",
						"inputType": "text",
						"fieldName": "keyMessage",
						"fieldTitle": "Key message",
						"validations": {
							"required": false
						}
					}
				}
			]
		},
		{
			"step": 7,
			"header": "Prompted Impressions instructions (for Indonesia studies only): ",
			"hidden": true,
			"parentId": "countries",
			"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
			"fields": [{
					"id": "Q19",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "grouped",
					"fieldName": "promptedImpressionsHidden",
					"fieldTitle": "Insert Prompted Impressions Instructions in the order of importance",
					"icon": "assets/images/modal_open.png",
					"totalCount": 6,
					"maxCount": 8,
					"description": " A minimum of 6 and no more than 8 prompted impressions can be included. No use of similar statements e.g. Has natural ingredients, has pure natural ingredients, each benefit should be mutually exclusive from the other. Avoid the use of very generic benefit statements along with specific statements e.g. It is effective or, is a trustworthy brand unless that is expressly and explicitly the objective of the copy to communicate. Your brand’s MWAs must be included at this question. MWAs are included in the Relevant Sample Master Sheet on the Unilever Portal. Additional messages on top of the MWAs should be included at this question to cover what was written in the creative brief as the main things that the ad should convey to consumers. The brand name (usually the variant) should NOT appear at the beginning of these statements.Follow the ‘Guidelines for writing Message Check and Prompted Impressions statements’ on PeopleWorld Knowledge Zone.",
					"subFields": [{
						"id": "Q19_1",
						"hidden": true,
						"parentId": "countries",
						"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
						"inputType": "text",
						"fieldName": "promptedImpressions",
						"fieldTitle": "Prompted Impressions Instructions",
						"validations": [{
							"name": "required",
							"value": true,
							"message": "Field cannot be empty."
						}]
					}]
				},
				{
					"id": "Q20",
					"inputType": "button",
					"fieldName": "",
					"fieldTitle": "Add More",
					"showBtn": true,
					"addMore": "promptedImpressionsHidden",
					"subFields": {
						"id": "Q19_1",
						"inputType": "text",
						"fieldName": "promptedImpressions",
						"fieldTitle": "Prompted Impressions Instructions",
						"reqMsg": "Field cannot be empty."
					}
				},
				{
					"id": "Q21",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "textarea",
					"fieldName": "adToConvey",
					"fieldTitle": "Now please tell us, in consumer language, what is the main communication point you want the ad to convey",
					"description": "We need to know what the main impression is that you want your ad to convey. We ask consumers to tell us in their own words what impressions the ad gave them about the brand. There are often many different ways of saying one given message, i.e. different words used to express the same thought. We need to add up all the different ways to get a true sense of how strongly the ad conveys that message. We need to know which ways of describing the main point of the ad should contribute to the ad successfully conveying the intended main point. As an example, the main thing we want the ad to convey is: Hellmann’s can be used in a variety of dishes. Expressions for code frame: Is versatile, Allows you to prepare many dishes, Inspires you about what to cook, Makes you think of what you can cook, Can be used in a variety of ways. ",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q22",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "text",
					"fieldName": "mainPoint",
					"fieldTitle": "Main point wording",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q23",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "text",
					"fieldName": "articulations",
					"fieldTitle": "OTHER articulations: (DESCRIPTION REQUIRED)",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q24",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"hasDependent": true,
					"inputType": "radio",
					"fieldTitle": "BCI Question wording",
					"fieldName": "isBciQuestion",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"description": "This question is used as a diagnosis of Authenticity, and measures the ability of the ad to be a good representation and expression of the overarching BCI. Try to include the brand name in your description e.g. Persil believe that children develop best when they are free to get dirty / There’s always a beautiful ending when you start with CIF.",
					"icon": "assets/images/modal_open.png"
				},
				{
					"id": "Q25",
					"hidden": true,
					"parentId": "isBciQuestion",
					"inputType": "select",
					"fieldName": "bciQuestion",
					"fieldTitle": "BCI Question wording",
					"condition": "this.studyForm.value['isBciQuestion'] == true",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}],
					"multi": false,
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum2"
						}
					]
				},
				{
					"id": "Q26",
					"hidden": true,
					"parentId": "isBciQuestion",
					"inputType": "text",
					"fieldName": "bciQuestion",
					"fieldTitle": "IF NO, specify BCI",
					"condition": "this.studyForm.value['isBciQuestion'] == false",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q27",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "select",
					"multi": true,
					"fieldName": "associationAttribute",
					"fieldTitle": "Insert Intutive Associations attributes in the order of importance",
					"icon": "assets/images/modal_open.png",
					"description": "This question is used as a diagnosis of Authenticity, and measures the ability of the ad to be a good representation and expression of the overarching BCI. Try to include the brand name in your description e.g. Persil believe that children develop best when they are free to get dirty / There’s always a beautiful ending when you start with CIF.",
					"service": "getAssociationAttribute",
					"validations": [{
							"name": "required",
							"value": true,
							"message": "Field cannot be empty."
						},
						{
							"name": "min",
							"value": 7,
							"message": "Minimum 7 attributes required."
						}
					],
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
						}
					]
				},
				{
					"id": "Q28",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "radio",
					"fieldTitle": "Action Standards confirmed with BPI ",
					"fieldName": "isConfirmedBpi",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				},
				{
					"id": "Q29",
					"hidden": true,
					"parentId": "countries",
					"condition": "this.studyForm.value['countries'].map(field => field.id).indexOf(21) != -1",
					"inputType": "radio",
					"fieldTitle": "BUDGET has been approved by CMI ",
					"fieldName": "isApprovedCmi",
					"validations": [{
						"name": "required",
						"value": true,
						"message": "Field cannot be empty."
					}]
				}
			]
		},
		{
			"step": 8,
			"header": "Key team members",
			"fields": [{
					"id": "Q30",
					"inputType": "grouped",
					"fieldTitle": "Marketing",
					"fieldName": "marketing",
					"subFields": [{
						"id": "Q30_2",
						"inputType": "email",
						"fieldTitle": "Email",
						"fieldName": "userEmail",
						"disabled": true,
						"validations": [{
							"name": "email",
							"value": true,
							"message": "Please enter a valid email."
						}],
						"autoFill": true
					}]
				},
				{
					"id": "Q31",
					"inputType": "grouped",
					"fieldTitle": "Marketing 2?",
					"fieldName": "marketing2",
					"subFields": [{
						"id": "Q31_2",
						"inputType": "email",
						"fieldTitle": "Email",
						"fieldName": "marketing2UserEmail",
						"validations": [{
								"name": "required",
								"value": true,
								"message": "Field cannot be empty."
							},
							{
								"name": "email",
								"value": true,
								"message": "Please enter a valid email."
							}
						]
					}]
				},
				{
					"id": "Q32",
					"inputType": "grouped",
					"fieldTitle": "CMI BPI",
					"fieldName": "cmiBpi",
					"subFields": [{
						"id": "Q32_2",
						"inputType": "email",
						"fieldTitle": "Email",
						"fieldName": "cmiBpiUserEmail",
						"validations": [{
								"name": "required",
								"value": true,
								"message": "Field cannot be empty."
							},
							{
								"name": "email",
								"value": true,
								"message": "Please enter a valid email."
							}
						]
					}]
				},
				{
					"id": "Q33",
					"inputType": "grouped",
					"fieldTitle": "HIVE",
					"fieldName": "hive",
					"subFields": [{
						"id": "Q33_2",
						"inputType": "email",
						"fieldTitle": "Email",
						"fieldName": "hiveUserEmail",
						"disabled": true,
						"validations": [{
							"name": "email",
							"value": true,
							"message": "Please enter a valid email."
						}]
					}]
				}
			]
		},
		{
			"step": 9,
			"header": "Attach brief",
			"fields": [{
				"id": "Q34",
				"inputType": "file",
				"fieldTitle": "Upload brief in Word / PDF format only",
				"fieldName": "attchBrief",
				"validations": [{
					"name": "required",
					"value": true,
					"message": "Field cannot be empty."
				}]
			}]
		}
	]
}

const adCertifierProductConfig = {
	"productId": 2,
	"productTitle": "AdCertifier",
	"components": [{
			"step": 1,
			"header": "First, some questions on the scope of the test",
			"fields": [{
					"id": "Q1",
					"inputType": "select",
					"fieldTitle": "Country of Fieldwork",
					"fieldName": "countries",
					"multi": true,
					"autofill": true,
					"service": "getAllCountries",
					"required": true,
					"reqMsg": "Field cannot be empty."
				},
				{
					"id": "Q2",
					"inputType": "number",
					"fieldTitle": "Number of Ads to be tested",
					"fieldName": "countOfAds",
					"max": 50,
					"failMsg": "Maximum 50 ads can be added",
					"required": true,
					"reqMsg": "Field cannot be empty."
				}
			]
		},
		{
			"step": 2,
			"header": "Now tell us about the project",
			"fields": [{
					"id": "Q3",
					"inputType": "text",
					"fieldTitle": "Unilever Project Name",
					"fieldName": "productName",
					"required": true,
					"reqMsg": "Field cannot be empty."
				},
				{
					"id": "Q4",
					"inputType": "select",
					"fieldTitle": "Category",
					"fieldName": "productCategory",
					"required": true,
					"multi": false,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum2"
						}
					]
				},
				{
					"id": "Q5",
					"inputType": "select",
					"fieldTitle": "Sub-Category",
					"fieldName": "productSubCategory",
					"selectedOptions": "this.studyForm.value['productCategory']",
					"required": true,
					"multi": false,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum2"
						}
					]
				},
				{
					"id": "Q6",
					"inputType": "select",
					"fieldTitle": "Global Brand name",
					"fieldName": "globalBrand",
					"autofill": true,
					"required": true,
					"multi": false,
					"reqMsg": "Field cannot be empty.",
					"service": "getAllBrands"
				},
				{
					"id": "Q7",
					"inputType": "select",
					"fieldTitle": "Local Parent Brand name",
					"subTitle": "(English)",
					"fieldName": "localBrandEng",
					"selectedOptions": "this.studyForm.value['globalBrand']",
					"required": true,
					"autofill": true,
					"multi": false,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum2"
						}
					]
				},
				{
					"id": "Q8",
					"inputType": "text",
					"fieldTitle": "Complete Variant brand name",
					"subTitle": "(English, Only If Applicable)",
					"fieldName": "completeBrandEng",
					"required": false
				}
			]
		},
		{
			"step": 3,
			"header": "Now please provide some details about each of the ads",
			"fields": [{
				"id": "Q9",
				"inputType": "grouped",
				"fieldTitle": "Ad {{index+1}} ",
				"fieldName": "ad_{{index+1}}",
				"totalCount": "this.studyForm.value['countOfAds']",
				"subFields": [{
						"id": "Q9_{{index+1}}",
						"inputType": "text",
						"fieldTitle": "Ad name",
						"fieldName": "adName",
						"required": true,
						"reqMsg": "Field cannot be empty."
					},
					{
						"id": "Q9_{{index+1}}",
						"inputType": "select",
						"fieldTitle": "Finished film or Unfinished",
						"icon": "assets/images/modal_open.png",
						"desciption": "Finished / Unfinished",
						"fieldName": "film",
						"multi": false,
						"required": true,
						"reqMsg": "Field cannot be empty.",
						"values": [{
								"id": 1,
								"name": "Finished film"
							},
							{
								"id": 2,
								"name": "Animatic"
							}
						]
					},
					{
						"id": "Q9_{{index+1}}",
						"inputType": "number",
						"fieldTitle": "Ad length ( in seconds)",
						"fieldName": "adLength",
						"required": true,
						"reqMsg": "Field cannot be empty.",
						"max": 300,
						"min": 5,
						"failMsg": "Please enter the value between 5-300."
					},
					{
						"id": "Q9_{{index+1}}",
						"inputType": "radio",
						"fieldTitle": "Has this ad been previously tested?",
						"fieldName": "isAdTested",
						"required": true,
						"reqMsg": "Field cannot be empty.",
						"values": [{
								"id": 1,
								"name": "Yes"
							},
							{
								"id": 2,
								"name": "No"
							}
						]
					},
					{
						"id": "Q9_{{index+1}}",
						"inputType": "text",
						"fieldTitle": "If Yes has it been tested in another market or same market?",
						"fieldName": "adTestedWhere",
						"condition": "this.studyForm.value['isAdTested'] == 1",
						"required": true,
						"reqMsg": "Field cannot be empty."
					}
				]
			}]
		},
		{
			"step": 4,
			"header": "And now for some inputs on research design",
			"fields": [{
					"id": "Q10",
					"inputType": "text",
					"fieldTitle": "Sample definition –",
					"fieldName": "sampleDefinition",
					"icon": "assets/images/modal_open.png",
					"description": "*Standard sample definitions are included in the Relevant Sample Master Sheet on the Unilever Portal",
					"autofill": true,
					"disabled": true
				},
				{
					"id": "Q11",
					"inputType": "radio",
					"fieldTitle": "Standard sample definition?",
					"fieldName": "wantStandardDefinition",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					// "values": [{
					// 		"id": 1,
					// 		"name": "Yes"
					// 	},
					// 	{
					// 		"id": 2,
					// 		"name": "No"
					// 	}
					// ]
				},
				{
					"id": "Q12",
					"inputType": "text",
					"fieldTitle": "Alternate Sample Definition",
					"fieldName": "alternateSampleDefinition",
					"condition": "this.studyForm.value['wantStandardDefinition'] == 2",
					"required": true,
					"reqMsg": "Field cannot be empty."
				},
				{
					"id": "Q13",
					"inputType": "select",
					"fieldTitle": "INDIA CEM to be reported -",
					"fieldName": "indiaCem",
					"condition": "this.studyForm.value['countries'].indexOf(17) != -1",
					"disabled": true,
					"multi": false,
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum2"
						}
					]
				},
				{
					"id": "Q14",
					"inputType": "select",
					"fieldTitle": "INDIA Region",
					"fieldName": "indiaReigon",
					"condition": "this.studyForm.value['countries'].indexOf(17) != -1",
					"icon": "true",
					"description": "Sample Size",
					"disabled": true,
					"multi": false,
					"values": [{
							"id": 1,
							"name": "Lorem Ipsum1"
						},
						{
							"id": 2,
							"name": "Lorem Ipsum2"
						}
					]
				},
				{
					"id": "Q15",
					"inputType": "radio",
					"fieldTitle": "Local Translations checked and approved",
					"fieldName": "areTranslationsChecked",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				}
			]
		},
		{
			"step": 5,
			"header": "Add On Modules",
			"fields": [{
					"id": "Q16",
					"inputType": "radio",
					"fieldTitle": "Include Position Zero",
					"fieldName": "includePositionZero",
					"icon": "assets/images/modal_open.png",
					"description": "LORUM IPSUM DOLAR",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				},
				{
					"id": "Q17",
					"inputType": "radio",
					"fieldTitle": "Agency Debrief / Consultancy required?",
					"fieldName": "isConsultancyRequired",
					"icon": "assets/images/modal_open.png",
					"description": "LORUM IPSUM DOLAR (*this will incur costs)",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				}
			]
		},
		{
			"step": 6,
			"header": "Key Database Details:",
			"fields": [{
					"id": "Q18",
					"inputType": "radio",
					"fieldTitle": "Is the ad a cut down version?",
					"fieldName": "isAdCutDown",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				},
				{
					"id": "Q19",
					"inputType": "radio",
					"fieldTitle": "Ad previously or currently on air in this market?",
					"fieldName": "isAdOnAir",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				},
				{
					"id": "Q20",
					"inputType": "radio",
					"fieldTitle": "Music especially created for brand?",
					"fieldName": "isMusicCreated",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				},
				{
					"id": "Q21",
					"inputType": "text",
					"fieldTitle": "Unilever Campaign Name",
					"fieldName": "unileverCampaignName",
					"required": true,
					"reqMsg": "Field cannot be empty."
				},
				{
					"id": "Q22",
					"inputType": "radio",
					"fieldTitle": "Ad ever tested as animatic/rough?",
					"fieldName": "isAdAnimaticTested",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"values": [{
							"id": 1,
							"name": "Yes"
						},
						{
							"id": 2,
							"name": "No"
						}
					]
				}
			]
		},
		{
			"step": 7,
			"header": "Key team members",
			"fields": [{
					"id": "Q23",
					"inputType": "grouped",
					"fieldTitle": "Marketing",
					"fieldName": "marketing",
					"subFields": [{
							"id": "Q23_1",
							"inputType": "text",
							"fieldTitle": "Name",
							"fieldName": "userName",
							"disabled": true,
							"autofill": true
						},
						{
							"id": "Q23_2",
							"inputType": "email",
							"fieldTitle": "Email",
							"fieldName": "userEmail",
							"disabled": true,
							"failMsg": "Please enter a valid email.",
							"autofill": true
						}
					]
				},
				{
					"id": "Q24",
					"inputType": "grouped",
					"fieldTitle": "Marketing 2?",
					"fieldName": "marketing2",
					"subFields": [{
							"id": "Q24_1",
							"inputType": "text",
							"fieldTitle": "Name",
							"fieldName": "marketing2UserName",
							"required": true,
							"reqMsg": "Field cannot be empty."
						},
						{
							"id": "Q24_2",
							"inputType": "email",
							"fieldTitle": "Email",
							"fieldName": "marketing2UserEmail",
							"required": true,
							"reqMsg": "Field cannot be empty.",
							"failMsg": "Please enter a valid email."
						}
					]
				},
				{
					"id": "Q25",
					"inputType": "grouped",
					"fieldTitle": "CMI BPI",
					"fieldName": "cmiBpi",
					"subFields": [{
							"id": "Q25_1",
							"inputType": "text",
							"fieldTitle": "Name",
							"fieldName": "cmiBpiUserName",
							"required": true,
							"reqMsg": "Field cannot be empty."
						},
						{
							"id": "Q25_2",
							"inputType": "email",
							"fieldTitle": "Email",
							"fieldName": "cmiBpiUserEmail",
							"required": true,
							"reqMsg": "Field cannot be empty.",
							"failMsg": "Please enter a valid email."
						}
					]
				},
				{
					"id": "Q26",
					"inputType": "grouped",
					"fieldTitle": "HIVE",
					"fieldName": "hive",
					"subFields": [{
							"id": "Q26_1",
							"inputType": "text",
							"fieldTitle": "Name",
							"fieldName": "hiveUserName",
							"disabled": true,
							"autofill": true
						},
						{
							"id": "Q26_2",
							"inputType": "email",
							"fieldTitle": "Email",
							"fieldName": "hiveUserEmail",
							"disabled": true,
							"failMsg": "Please enter a valid email.",
							"autofill": true
						}
					]
				},
				{
					"id": "Q27",
					"inputType": "grouped",
					"fieldTitle": "Agency Contact",
					"fieldName": "agencyContact",
					"disabled": true,
					"subFields": [{
							"id": "Q27_1",
							"inputType": "text",
							"fieldTitle": "Name",
							"fieldName": "agencyContactUserName",
							"required": true,
							"reqMsg": "Field cannot be empty."
						},
						{
							"id": "Q27_2",
							"inputType": "email",
							"fieldTitle": "Email",
							"fieldName": "agencyContactUserEmail",
							"required": true,
							"reqMsg": "Field cannot be empty.",
							"failMsg": "Please enter a valid email."
						}
					]
				},
				{
					"id": "Q28",
					"inputType": "date",
					"fieldTitle": "Date of ad delivery",
					"fieldName": "dateOfDelivery",
					"required": true,
					"reqMsg": "Field cannot be empty.",
					"disablePreviousDate": true,
					"failMsg": "Please select a valid date."
				}
			]
		},
		{
			"step": 8,
			"header": "Ratecard",
			"fields": [{
				"id": "Q29",
				"inputType": "text",
				"fieldTitle": "Ratecard price: (CALCULATED)",
				"fieldName": "rateCardPrice",
				"disabled": true,
				"rule": ""
			}]
		},
		{
			"step": 9,
			"header": "Unilever Do Number",
			"fields": [{
				"id": "Q30",
				"inputType": "number",
				"fieldTitle": "Do Number",
				"fieldName": "unileverDoNumber",
				"required": true,
				"reqMsg": "Field cannot be empty."
			}]
		}
	]
}

v.addSchema(componentSchema,'/componentSchema');
v.addSchema(fieldsSchema,'/fieldsSchema');
v.addSchema(commonInputsSchema,'/commonInputsSchema');
v.addSchema(radioInputSchema,'/radioInputSchema');
v.addSchema(groupedInputSchema,'/groupedInputSchema');

const productConfigs = [previewProductConfig,adCertifierProductConfig];

for (config of productConfigs) {
	const validatedStructure = v.validate(config, productStructureSchema);
	for (error of validatedStructure.errors) {
		console.log("Validated Structure Errors : ",error);
	}
	validateComponents(validatedStructure);
}

function validateComponents(validatedStructure){
	for (component of validatedStructure.instance.components){
		const validatedComponents = v.validate(component, componentSchema)
		for (error of validatedComponents.errors) {
			console.log("Validated Component Errors : ",error);
		}
		validateComponentFields(validatedComponents);
	}
}

function validateComponentFields(validatedComponents){
	for (field of validatedComponents.instance.fields){
		validateFields(field);
	}
}

function validateFields(field){
	if(field.inputType === 'text' || field.inputType === 'textarea' || field.inputType === 'number' || 
		field.inputType === 'date' || field.inputType === 'email' || field.inputType === 'select'){
		const validatedCommonFields = v.validate(field, commonInputsSchema)
		for (error of validatedCommonFields.errors){
			console.log("Validated Common Input Errors : ",error)
		}
	} 
	else if (field.inputType === 'radio'){
		const validatedRadioFields = v.validate(field, radioInputSchema)
		for (error of validatedRadioFields.errors){
			console.log("Validated Radio Input Errors : ",error)
		}
	}
	else if (field.inputType === 'grouped'){
		const validatedGroupedFields = v.validate(field, groupedInputSchema)
		for (error of validatedGroupedFields.errors){
			console.log("Validated Grouped Input Errors : ",error)
		}
		if(validatedGroupedFields.errors.length === 0){
			for(subfield of field.subFields){
				validateFields(subfield);
			}
		}
	}
	else if (field.inputType === 'file'){
		const validatedFileFields = v.validate(field, radioInputSchema)
		for (error of validatedFileFields.errors){
			console.log("Validated File Input Errors : ",error)
		}
	}
}