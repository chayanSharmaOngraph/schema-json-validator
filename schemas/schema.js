const commonFields = {
    'id': {
      'type': ['number','string'],
      'required': true
    },
    'inputType': {
      'type': 'string',
      'enum': ['text', 'textarea','select','number','grouped','radio','date','button','email','file']
    },
    'fieldTitle': {
      'type': 'string',
      'required': true
    },
    'fieldName': {
      'type': 'string',
      'required': true
    }
  }
  
  const booleanFields = {
    'required': {
      'type': 'boolean',
    },
    'multi': {
      'type': 'boolean',
    },
    'autofill': {
      'type': 'boolean',
    },
    'disabled': {
      'type': 'boolean',
    },
    'disablePreviousDate': {
      'type': 'boolean'
    }
  }
  
  const numberFields = {
    'totalCount': {
      'type': ['number','string'],
      'minimum': 1
    },
    'max': {
      'type': 'number',
      'minimum': 1
    },
    'min': {
      'type': 'number',
      'minimum': 1
    },
    'subCount': {
      'type': 'number',
      'minimum': 1
    },
    'dependencies': {
      'subCount': 'totalCount'
    }
  }
  
  const stringFields = {
    "subTitle": {
      'type': 'string',
    },
    'reqMsg': {
      'type': 'string',
    },
    'service': {
      'type': 'string',
    },
    'selectedOptions': {
      'type': 'string',
    },
    'icon': {
      'type': 'string',
    },
    'desciption': {
      'type': 'string',
    },
    'condition': {
      'type': 'string',
    },
    'failMsg': {
      'type': 'string'
    },
    'rule': {
      'type': 'string'
    },
    'dependencies': {
      'icon': 'desciption'
    }
  }
    
  const arrayFields = {
    'values': {
      'type': 'array',
      'minItems': 1,
      'uniqueItems': true,
      'items': {
        'type': 'object',
        'minProperties': 2,
        'properties': {
          'id': {
            'type': 'number',
            'required': true
          },
          'name': {
            'type': 'string',
            'required': true
          },
          'dependencies': {
            'id': 'name'
          },
          'dependencies': {
            'name': 'id'
          }
        }
      }
    }
  }
    
  const optionalFields = {
    ...booleanFields,
    ...numberFields,
    ...stringFields,
    ...arrayFields,
    'dependencies': {
      'required': 'reqMsg'
    },
    'dependencies': {
      'max': 'failMsg'
    },
    'dependencies': {
      'min': 'failMsg'
    },
    'dependencies': {
      'subTitle': 'fieldTitle'
    }
  }
  
  const commonInputsSchema = {
    'id': '/commonInputsSchema',
    'type': 'object',
    'properties': {
      ...commonFields,
      ...optionalFields
    }
  };
  
  const radioInputSchema = {
    'id': '/radioInputSchema',
    'type': 'object',
    'properties': {
      ...optionalFields,
      ...commonFields,
      'values': {
        'type': 'array',
        'required': true,
        'minItems': 1,
        'uniqueItems': true,
        'items': {
          'type': 'object',
          'minProperties': 2,
          'properties': {
            'id': {
              'type': 'number',
              'required': true
            },
            'name': {
              'type': 'string',
              'required': true
            },
            'dependencies': {
              'id': 'name'
            },
            'dependencies': {
              'name': 'id'
            }
          }
        }
      }
    }
  }
    
  const groupedInputSchema = {
    'id': '/groupedInputSchema',
    'type': 'object',
    'properties': {
      'subFields': {
        'type': 'array',
        'items': {'$ref': '/fieldsSchema'},
        'minItems': 1,
        'required': true
      },
      ...commonFields,
      ...optionalFields
    }
  };
  
  const fieldsSchema = {
    'id': '/fieldsSchema',
    'type': 'object',
    'minProperties': 1,
    'properties': {
      ...commonFields,
      ...optionalFields
    }
  }
  
  const componentSchema = {
    'id': '/componentSchema',
    'type': 'object',
    'minProperties': 3,
    'properties': {
      'step': {
        'type': 'number',
        'required': true,
        'minimum': 1
      },
      'header': {
        'type': 'string',
        'required': true
      }
      ,
      'fields': {
        'type': 'array',
        'items': {'type': 'object'},
        'minItems': 1,
        'required': true
      }
    }
  }
  
  
  const productStructureSchema = {
    'id': '/productStructureSchema',
    'type' : 'object',
    'minProperties': 3,
    'properties': {
      'productId': {
        'type': 'number',
        'required': true,
        'minimum': 1
      },
      'productTitle': {
        'type': 'string',
        'required': true
      },
      'components': {
        'type': 'array',
        'items': {'type': 'object'},
        'minItems': 1,
        'required': true
      }
    }
  }
  
  module.exports = {
    productStructureSchema,
    componentSchema,
    commonInputsSchema,
    radioInputSchema,
    groupedInputSchema,
    fieldsSchema
};